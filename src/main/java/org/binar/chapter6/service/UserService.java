package org.binar.chapter6.service;

import org.binar.chapter6.model.UserActive;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    void addUser(UserActive userActive);

    void updateUser(String username, String email, String password) throws Exception;

    void deleteUserById(Integer userActive) throws Exception;
}
