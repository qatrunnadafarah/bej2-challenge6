package org.binar.chapter6.model;

import javax.persistence.*;

@Embeddable
public class SeatId {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer seatID;
}
