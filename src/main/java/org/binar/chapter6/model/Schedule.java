package org.binar.chapter6.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@Entity
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer scheduleID;

    @OneToMany
    @JoinColumn(name = "film_id")
    private List<Film> filmID;

    private LocalDateTime showDate;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Long price;

}
