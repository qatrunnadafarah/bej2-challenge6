package org.binar.chapter6.repository;

import org.binar.chapter6.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends JpaRepository<Film, Integer> {

    // query biasa, isi query ngikutin object di java
    @Query(value = "SELECT * FROM Film WHERE schedule = :schedule")
    List<Film> findFilmBySchdl(@Param("schedule") Integer schedule);

    @Query(value = "SELECT * FROM Film WHERE title = :title")
    List<Film> findFilmByTitle(@Param("title") String title);

    @Query(value = "SELECT * FROM Film WHERE filmID = :filmID")
    List<Film> deleteFilmById(@Param("filmID") Integer filmID);

}
