package org.binar.chapter6.service;

import org.binar.chapter6.model.UserActive;
import org.binar.chapter6.repository.UserActiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserActiveRepository userActiveRepository;

    @Override
    public void addUser(UserActive userActive) {
        userActiveRepository.save(userActive);
    }

    @Override
    public void updateUser(String username, String email, String password) throws Exception {

    }

    @Override
    public void deleteUserById(Integer userActive) throws Exception {
        userActiveRepository.deleteById(userActive);
    }
}
