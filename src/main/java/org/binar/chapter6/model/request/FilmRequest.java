package org.binar.chapter6.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmRequest {

    private String title;

    private String schedule;

    private Boolean sedangTayang;
}
