package org.binar.chapter6.repository;

import org.binar.chapter6.model.Seat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SeatRepositoryTest {

    @Autowired
    SeatRepository seatRepository;

    @Test
    void testAddSeat() {
        Seat seat = new Seat();
        seat.setStudioName("A");

        seatRepository.save(seat);
    }
}
