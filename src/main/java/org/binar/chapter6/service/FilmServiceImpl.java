package org.binar.chapter6.service;

import org.binar.chapter6.model.Film;
import org.binar.chapter6.model.response.FilmResponse;
import org.binar.chapter6.repository.FilmRepository;
import org.binar.chapter6.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    FilmRepository filmRepository;
    ScheduleRepository scheduleRepository;

    @Override
    public void addFilm(Film film) throws Exception {
        filmRepository.save(film);
    }

    @Override
    public void updateFilm(String title, Integer filmID) throws Exception {

    }

    @Override
    public void deleteFilm(Integer filmID) {
        filmRepository.deleteById(filmID);
    }

    @Override
    public List<Film> getAllFilms() {
        return null;
    }

    @Override
    public List<FilmResponse> getScheduleByFilmId(String filmId) {

        return null;
    }

    @Override
    public Film searchFilmBySchedule(Integer schedule) {
        return filmRepository.findFilmBySchdl(schedule).get(0);
    }

    @Override
    public Film searchFilmByID(Integer filmID) {
        return null;
    }

    @Override
    public Film searchFilmByTitle(String title) {
        return (Film) filmRepository.findFilmByTitle(title);
    }

}
