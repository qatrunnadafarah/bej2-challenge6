package org.binar.chapter6.model.enumerations;

public enum Roles {

    CUSTOMER, ADMIN, SELLER
}
