package org.binar.chapter6.repository;

import org.binar.chapter6.model.UserActive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository //langsung dibikin bean cmn blm terhubung. pakai autowired ke repositorytest
public interface UserActiveRepository extends JpaRepository<UserActive, Integer> { //bikin model ke tabel mana, PK


}
