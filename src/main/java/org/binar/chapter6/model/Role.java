package org.binar.chapter6.model;

import lombok.Data;
import org.binar.chapter6.model.enumerations.Roles;

import javax.persistence.*;

@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer RoleId;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Roles name;
}
